package xesj.spring.validation;
import xesj.tool.StringTool;

/**
 * Text --&gt; String konverziót megvalósító osztály.
 * <pre>
 * null  --&gt; null
 * ""    --&gt; null
 * "..." --&gt; "..."  
 * </pre>
 */
public class StringConvert extends Convert {
  
  /**
   * Konstruktor, konverzió.
   * @param text A konvertálandó adat.
   */
  public StringConvert(String text) {
    if (!StringTool.equals(text, "")) {
      value = text;
    }
  }
  
  // =====
}

package xesj.spring.validation;
import lombok.Getter;

/**
 * BaseContext osztály által kiváltott exception, mely tartalmazza a BaseContext objektumot.
 */
@Getter
public class BaseContextException extends RuntimeException {
  
  private BaseContext baseContext;
  
  /**
   * Konstruktor
   */
  public BaseContextException(BaseContext baseContext) {
    super(baseContext.toString());
    this.baseContext = baseContext;
  }

  /**
   * BaseContext lekérdezése
   */  
  public BaseContext getBaseContext() {
    return baseContext;
  }
  
  // =====
}

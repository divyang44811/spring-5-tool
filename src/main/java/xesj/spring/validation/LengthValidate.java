package xesj.spring.validation;
import xesj.tool.StringTool;

/**
 * String típusú adat hosszának ellenőrzése.
 */
public class LengthValidate extends Validate {
  
  /**
   * Konstruktor, validáció.<br/>
   * Érvénytelen: ha az adat nem null, nem üres string, és a hosszúsága nincs a megadott intervallumban.<br/>
   * Érvényes: Ha az adat null, vagy üres string, vagy a hosszúsága a megadott intervallumban van.
   * @param data A vizsgálandó adat.
   * @param minimum A lehetséges minimális hosszúság. Ha nincs minimum, akkor null. 
   * @param maximum A lehetséges maximális hosszúság. Ha nincs maximum, akkor null. 
   */
  public LengthValidate(String data, Long minimum, Long maximum) {  
    
    // Van adat ?
    if (StringTool.isNullOrEmpty(data)) {
      return;
    }
    
    // A minimális hosszúság ellenőrzése
    if (minimum != null && data.length() < minimum) {
      message = new Message(
        "xesj.spring.validation.LengthValidate.minimum", 
        new String[]{String.valueOf(data.length()), minimum.toString()}, 
        null
      );
    }

    // A maximális hosszúság ellenőrzése
    if (maximum != null && data.length() > maximum) {
      message = new Message(
        "xesj.spring.validation.LengthValidate.maximum", 
        new String[]{String.valueOf(data.length()), maximum.toString()}, 
        null
      );
    }
  }
  
  // =====
}

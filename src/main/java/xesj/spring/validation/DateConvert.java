package xesj.spring.validation;
import java.text.ParseException;
import lombok.NonNull;
import xesj.tool.DateTool;
import xesj.tool.StringTool;

/**
 * Text --&gt; java.util.Date konverziót megvalósító osztály.
 */
public class DateConvert extends Convert {
  
  /**
   * Konstruktor, konverzió.
   * @param text A konvertálandó adat.
   * @param pattern A konvertálandó adat dátum formátuma.
   * @param patternDesc Dátum formátum leírása a felhasználóknak érthető formában. 
   *                    A hibaüzenet kiíráskor van szerepe, de csak akkor jelenik meg ha a "message code plus" be van állítva "+patternDesc" értékre.
   *                    Lehet null is.
   */
  public DateConvert(String text, @NonNull String pattern, String patternDesc) {

    // Ha nincs konvertálandó adat, akkor a konverzió eredménye: null
    if (StringTool.isNullOrEmpty(text)) {
      return;
    }
    
    // Konverzió
    try {
      value = DateTool.parse(text, pattern);
    }
    catch (ParseException pe) {
      message = new Message(
        "xesj.spring.validation.DateConvert", 
        new String[]{pattern, patternDesc}, 
        null
      );
    } 
  }

  /**
   * Konstruktor, konverzió.
   * @param text A konvertálandó adat.
   * @param pattern A konvertálandó adat dátum formátuma.
   */
  public DateConvert(String text, @NonNull String pattern) {
    this(text, pattern, null);
  }
 
  // =====
}

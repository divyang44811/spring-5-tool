package xesj.spring.validation;
import java.util.ArrayList;
import java.util.List;
import lombok.NonNull;
import xesj.tool.StringTool;

/**
 * Konverzió/validáció kontextus általános esetre (nem html űrlapok esetére).
 * A hibaüzeneteket mindig a kontextusba kerülésük sorrendjében adja vissza.
 * A mezőneveket mindig a kontextusba kerülésük sorrendjében adja vissza.
 * Tartalmaz egy boolean típusú FLAG-et, ami beállítható, illetve lekérdezhető.
 * Amikor a kontextusba hibaüzenet keletkezik, akkor a kontextus a FLAG-et false értékre állítja be.
 */
public class BaseContext {
  
  private boolean fieldUnique;
  private boolean flag;
  private MessageSourceLocale msl;
  private Long throwException;
  private List<String> fields;
  private List<String> messages;
  private String comment;
  
  /**
   * Konstruktor
   * @param msl Hibaüzeneteket visszafejtő objektum. 
   * @param fieldUnique Egy mezőnévhez maximum egy üzenet kerülhessen be a kontextusba ?
   * @param throwException Meghatározza, hogy hány hibaüzenetnek kell a BaseContext-be kerülni ahhoz, hogy BaseContextException-t dobjon.
   *                       Null esetén nem dob BaseContextException-t. Minimális értéke: 1      
   */
  public BaseContext(@NonNull MessageSourceLocale msl, boolean fieldUnique, Long throwException) {
    // Ellenőrzés
    if (throwException != null) {
      if (throwException < 1) {
        throw new RuntimeException("A throwException paraméternek legalább 1-nek kell lennie!");
      }
    }
      
    // Beállítás  
    this.msl = msl;
    this.fieldUnique = fieldUnique;
    this.throwException = throwException;
    fields = new ArrayList<>();
    messages = new ArrayList<>();
    flag = true;
  }
  
  /**
   * Validáció hozzáadása a kontextushoz.
   * @param field Hibaüzenet hozzárendelése ehhez a mezőnévhez. Null esetén a hibaüzenet globális lesz. 
   * @param validate Validáció.
   */
  public void add(String field, @NonNull Validate validate) {
    add(field, validate.getMessage(msl));    
  }

  /**
   * Konverzió hozzáadása a kontextushoz.
   * @param field Hibaüzenet hozzárendelése ehhez a mezőnévhez. Null esetén a hibaüzenet globális lesz. 
   * @param convert Konverzió.
   */
  public void add(String field, @NonNull Convert convert) {
    add(field, convert.getMessage(msl));    
  }

  /**
   * Hibaüzenet objektum hozzáadása a kontextushoz.
   * @param field Hibaüzenet hozzárendelése ehhez a mezőnévhez. Null esetén a hibaüzenet globális lesz. 
   * @param message Hibaüzenet objektum
   */
  public void add(String field, @NonNull Message message) {
    add(field, message.getMessageText(msl));    
  }
  
  /**
   * Hibaüzenet szöveg hozzáadása a kontextushoz.
   * @param field Hibaüzenet hozzárendelése ehhez a mezőnévhez. Null esetén a hibaüzenet globális lesz. 
   * @param message Hibaüzenet.
   */
  public void add(String field, String message) {
    // Field és message paraméter ellenőrzése
    checkField(field);
    checkMessage(message);

    // Van hibaüzenet ?
    if (message == null) {
      return;
    }
    else {
      flag = false;
    }  

    // Ha fieldUnique módban vagyunk, és van már hibaüzenet a mezőhöz, nincs további teendő
    if (fieldUnique) {
      if (field != null) { 
        if (fields.contains(field)) {
          return; // Nincs teendő
        }
      }  
    } 
    
    // Hibaüzenet bejegyzése
    fields.add(field);
    messages.add(message);
    
    // Ha a hibaüzenetek száma elérte a throwException-nel beállított értéket, akkor BaseContextException-t kell dobni
    if (throwException != null) {
      if (messages.size() >= throwException) {
        throw new BaseContextException(this);
      }  
    }
  }
  
  /**
   * FLAG beállítása true vagy false állapotra. Semmi másra nincs hatással csak a FLAG-re!
   * Ez a beállítás akkor hasznos, ha a későbbiekben a FLAG le van vizsgálva a getFlag() metódussal.
   * Így kiderül, hogy a setFlag(true), és getFlag() között keletkezett-e hibaüzenet (a fieldUnique beállítástól függetlenül). 
   */
  public void setFlag(boolean flag) {
    this.flag = flag;
  }

  /**
   * FLAG állapotának lekérdezése.
   * @see #setFlag(boolean)
   */
  public boolean getFlag() {
    return flag;
  }
  
  /**
   * A kontextusban lévő mezőnevek lekérdezése, a kontextusba kerülésük sorrendjében.
   * Egy mezőnév csak egyszer szerepel a listában, és a lista nem tartalmaz null-t.  
   * Ha nincs egyetlen mezőnév sem, akkor üres listát ad vissza.
   */
  public List<String> getFields() {
    List<String> result = new ArrayList<>();
    for (String f: fields) {
      if (f != null) {
        if (!result.contains(f)) {
          result.add(f);
        }
      }    
    }
    return result;
  }
  
  /**
   * Az összes mezőnév lekérdezése (a null-t is beleértve), a kontextusba kerülésük sorrendjében.
   * Egy mezőnév többször is szerepelhet. Ha nincsenek mezőnevek, akkor üres listát ad vissza.
   * A getAllFields(), getAllMessages() metódusok együttes használatával a BaseContext teljes belső hibaüzenet tárolója lekérdezhető,
   * az általuk kapott listák elemszáma megegyezik, a listák azonos indexein találhatók a mezőnév-hibaüzenet párok. 
   */
  public List<String> getAllFields() {
    return fields;
  }  

  /**
   * Az összes hibaüzenet (a mezőhöz rendeltek, és a globálisak) lekérdezése, a kontextusba kerülésük sorrendjében.
   * Ha nincs hibaüzenet, akkor üres listát ad vissza.
   * A getAllFields(), getAllMessages() metódusok együttes használatával a BaseContext teljes belső hibaüzenet tárolója lekérdezhető,
   * az általuk kapott listák elemszáma megegyezik, a listák azonos indexein találhatók a mezőnév-hibaüzenet párok. 
   */
  public List<String> getAllMessages() {
    return messages;
  }
  
  /**
   * Globális hibaüzenetek lekérdezése, a kontextusba kerülésük sorrendjében.
   * Ha nincs ilyen, akkor üres listát ad vissza.
   */
  public List<String> getGlobalMessages() {
    List<String> result = new ArrayList<>();
    for (int i = 0; i < fields.size(); i++) {
      if (fields.get(i) == null) {
        result.add(messages.get(i));
      }    
    }
    return result;
  }

  /**
   * Mezőhöz tartozó hibaüzenetek lekérdezése, a kontextusba kerülésük sorrendjében.
   * Ha nincs ilyen, akkor üres listát ad vissza.
   * @param field Mezőnév.
   */
  public List<String> getFieldMessages(@NonNull String field) {
    checkField(field);
    List<String> result = new ArrayList<>();
    for (int i = 0; i < fields.size(); i++) {
      if (StringTool.equals(fields.get(i), field)) {
        result.add(messages.get(i));
      }    
    }
    return result;
  }
  
  /**
   * Létezik-e bármilyen hibaüzenet a kontextusban ?
   */
  public boolean hasMessage() {
    return !fields.isEmpty();
  }

  /**
   * Létezik-e bármilyen mezőhöz tartozó hibaüzenet a kontextusban ?
   */
  public boolean hasFieldMessage() {
    for (String f: fields) {
      if (f != null) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Létezik-e az adott mezőnévhez tartozó hibaüzenet a kontextusban ?
   * @param field Mezőnév
   */
  public boolean hasFieldMessage(@NonNull String field) {
    checkField(field);
    return fields.contains(field);
  }
  
  /**
   * Létezik-e bármilyen globális (nem mezőhöz tartozó) hibaüzenet a kontextusban ?
   */
  public boolean hasGlobalMessage() {
    return fields.contains(null);
  }

  /**
   * Mezőnév ellenőrzése: nem lehet üres string.
   * @throws RuntimeException Ha a mezőnév üres string.
   */
  private void checkField(String field) {
    if (StringTool.equals(field, "")) {
      throw new RuntimeException("A 'field' paraméter nem lehet üres string!");
    }
  }
  
  /**
   * Hibaüzenet ellenőrzése: nem lehet üres string.
   * @throws RuntimeException Ha a hibaüzenet üres string.
   */
  private void checkMessage(String message) {
    if (StringTool.equals(message, "")) {
      throw new RuntimeException("A 'message' paraméter nem lehet üres string!");
    }
  }
  
  /**
   * Megjegyzés beállítása a BaseContext-hez. A BaseContext működésére nincs hatással, csak információs célokat szolgál.
   * Akkor érdemes a metódust használni, ha később a getComment()-tel lekérdezzük a megjegyzést, és így visszanyerjük az információt.
   * @param comment Megjegyzés
   */
  public void setComment(String comment) {
    this.comment = comment;
  }

  /**
   * Megjegyzés lekérdezése. Visszanyerhető vele az az információ, ami korábban a setComment() metódussal be lett állítva.
   * @return Az a megjegyzés, ami a setComment() metódussal előzőleg be lett állítva.
   */
  public String getComment() {
    return comment;
  }
  
  /**
   * BaseContextException dobása ha a BaseContext tartalmaz hibaüzenetet (piszkos).
   * Ha a BaseContext nem tartalmaz hibaüzenetet (tiszta), akkor a metódusnak nincs hatása.
   */
  public void throwExceptionIfDirty() throws BaseContextException {
    if (hasMessage()) {
      throw new BaseContextException(this);
    }
  }
  
  /**
   * A BaseContext mezőinek, és hibaüzeneteinek string-re alakítása
   */  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < fields.size(); i++) {
      if (sb.length() > 0) sb.append('\n');
      if (fields.get(i) == null) {
        sb.append(messages.get(i));
      }
      else {
        sb.append(fields.get(i)).append(": ").append(messages.get(i));
      }
    }
    return sb.toString();
  }

  // =====  
}

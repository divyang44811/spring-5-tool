package xesj.spring.validation;
import lombok.NonNull;

/**
 * Konverzió ősosztály. 
 * Konverzió készítéskor ezt az osztályt kell leszármaztatni, és a leszármazott osztálynak csak a konstruktorát kell megírni.
 * A konstruktornak el kell végezni a konverziót.
 * Sikeres konverzió esetén az eredményt a "value" változóba kell tárolni.
 * Adathiba miatti sikertelen konverzió esetén a "message" változóba be kell állítani a hibaüzenetet, és a "value" változót nem kell kitölteni.
 */
public class Convert {
  
  /** Sikeres konverzió esetén a konverzió eredménye, különben null */
  protected Object value;

  /** Sikertelen konverzió esetén a hibaüzenet, különben null */
  protected Message message;
  
  /**
   * Hozzáadás a messages.properties fájlban lévő kódhoz, hogy a hibaüzenet szöveg meghatározásakor a Spring ezzel az új kóddal keressen.
   * @param plus A messages.properties fájlban lévő kódhoz hozzáfűzendő string. Ajánlott a stringet "+" jellel kezdeni. Null esetén nem változik a kód.
   * @return Saját maga.
   */
  public Convert messageCodePlus(String plus) {
    if (message != null) {
      message.messageCodePlus(plus);
    }  
    return this;
  }
  
  /**
   * A hibaüzenet elé írandó szöveg beállítása.
   * @param prefix A hibaüzenet elé írandó szöveg. Null esetén nem változik a hibaüzenet.
   * @return Saját maga.
   */
  public Convert messagePrefix(String prefix) {
    if (message != null) {
      message.messagePrefix(prefix);
    }
    return this;
  }

  /**
   * A hibaüzenet után írandó szöveg beállítása.
   * @param postfix A hibaüzenet után írandó szöveg. Null esetén nem változik a hibaüzenet.
   * @return Saját maga.
   */
  public Convert messagePostfix(String postfix) {
    if (message != null) {
      message.messagePostfix(postfix);
    }  
    return this;
  }
  
  /**
   * A konverzió során keletkezett hibaüzenet lekérdezése. Ha nem keletkezett hibaüzenet akkor null-t ad vissza.
   * @param msl Hibaüzeneteket visszafejtő objektum.
   * @return A hibaüzenet. Ha nem keletkezett hibaüzenet akkor null.
   */
  public String getMessage(@NonNull MessageSourceLocale msl) {
    return (message == null ? null : message.getMessageText(msl));
  }
  
  /**
   * A konverzió eredményének lekérdezése. 
   * Ezt a lekérdezést csak akkor szabad végrehajtani, ha a konverzió közben nem keletkezett hibaüzenet, különben RuntimeException keletkezik. 
   * A kapott eredményt CAST-olni szükséges a konverziótól függően. Példa:<br/><br/>
   * <pre>
   * LongConvert convert = new LongConvert(...);
   * if (convert.isValid()) {
   *   Long value = (Long)convert.getValue();
   * }
   * </pre>
   * @return A konverzió eredménye.
   */
  public Object getValue() {
    if (message != null) {
      throw new RuntimeException("Sikertelen konverzió esetén nem kérdezhető le a konverzió eredménye!");
    }
    return value;
  }
  
  /**
   * Konverzió sikerességének ellenőrzése.
   * @return True: sikeres konverzió, nem keletkezett hibaüzenet.<br/>
   *         False: sikertelen konverzió, keletkezett hibaüzenet.
   */
  public boolean isValid() {
    return (message == null);
  }

  // =====
}

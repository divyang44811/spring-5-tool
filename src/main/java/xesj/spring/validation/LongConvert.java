package xesj.spring.validation;
import java.math.BigInteger;
import xesj.tool.StringTool;

/**
 * Text --&gt; Long konverziót megvalósító osztály.
 */
public class LongConvert extends Convert {
  
  /**
   * Konstruktor, konverzió.
   * @param text A konvertálandó adat.
   */
  public LongConvert(String text) {
    
    // Van adat ?
    if (StringTool.isNullOrEmpty(text)) {
      return;
    }
    
    // Konverzió
    try {
      value = Long.parseLong(text);
    }
    catch (NumberFormatException nfe1) {
      // Nem lehet Long-ra konvertálni, de lehetséges hogy BigInteger-be lehet.
      try {
        new BigInteger(text);
        message = new Message(
          "xesj.spring.validation.LongConvert.interval", 
          new Long[]{Long.MIN_VALUE, Long.MAX_VALUE}, 
          null
        );
      }
      catch (NumberFormatException nfe2) {
        message = new Message("xesj.spring.validation.LongConvert", null, null);
      }
    } 
  }
 
  // =====
}

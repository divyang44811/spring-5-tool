package xesj.spring.validation;
import java.math.BigDecimal;

/**
 * java.math.BigDecimal típusú adat ellenőrzése: a megadott intervallumban van-e ?
 */
public class BigDecimalRangeValidate extends Validate {

  /**
   * Konstruktor, validáció.<br/>
   * Érvénytelen: ha az adat nem null, és nincs a megadott intervallumon belül.<br/> 
   * Érvényes: ha az adat null, vagy a megadott intervallumon belül van.
   * @param data A vizsgálandó adat.
   * @param minimum A lehetséges minimum érték. Ha nincs minimum, akkor null. 
   * @param maximum A lehetséges maximum érték. Ha nincs maximum, akkor null. 
   */
  public BigDecimalRangeValidate(BigDecimal data, BigDecimal minimum, BigDecimal maximum) {  

    // Van adat ?
    if (data == null) {
      return;
    }

    // Validáció a minimum értékre
    if (minimum != null && data.compareTo(minimum) == -1) {
      message = new Message(
        "xesj.spring.validation.BigDecimalRangeValidate.minimum", 
        new String[]{minimum.toString()}, 
        null
      );
    }

    // Validáció a maximum értékre
    if (maximum != null && data.compareTo(maximum) == 1) {
      message = new Message(
        "xesj.spring.validation.BigDecimalRangeValidate.maximum", 
        new String[]{maximum.toString()}, 
        null
      );
    }
  }

  // =====
}

package xesj.spring.validation;
import lombok.NonNull;
import org.springframework.validation.BindingResult;
import xesj.tool.StringTool;

/**
 * Konverzió/validáció kontextus html űrlapok esetére.
 */
public class FormContext {
  
  private BindingResult bindingResult;
  private boolean fieldUnique;
  private MessageSourceLocale msl;
  
  /**
   * Konstruktor.
   * @param bindingResult Az űrlap post-olásakor a Spring által készített BindingResult objektum.
   * @param msl Hibaüzeneteket visszafejtő objektum.
   * @param fieldUnique 
   *        True: egy mezőnévhez csak a legelső hibaüzenet kerülhet be a kontextusba. 
   *        False: egy mezőnévhez tetszőleges sok hibaüzenet kerülhet be a kontextusba.
   */
  public FormContext(@NonNull BindingResult bindingResult, @NonNull MessageSourceLocale msl, boolean fieldUnique) {
    this.bindingResult = bindingResult;
    this.msl = msl;
    this.fieldUnique = fieldUnique;
  }

  /**
   * Validáció hozzáadása a kontextushoz.
   * @param field Hibaüzenet hozzárendelése ehhez a mezőnévhez. Null esetén a hibaüzenet globális lesz. 
   * @param validate Validáció.
   */
  public void add(String field, @NonNull Validate validate) {
    add(field, validate.getMessage(msl));    
  }

  /**
   * Konverzió hozzáadása a kontextushoz.
   * @param field Hibaüzenet hozzárendelése ehhez a mezőnévhez. Null esetén a hibaüzenet globális lesz. 
   * @param convert Konverzió.
   */
  public void add(String field, @NonNull Convert convert) {
    add(field, convert.getMessage(msl));    
  }

  /**
   * Hibaüzenet objektum hozzáadása a kontextushoz.
   * @param field Hibaüzenet hozzárendelése ehhez a mezőnévhez. Null esetén a hibaüzenet globális lesz. 
   * @param message Hibaüzenet objektum
   */
  public void add(String field, @NonNull Message message) {
    add(field, message.getMessageText(msl));    
  }
  
  /**
   * Hibaüzenet szöveg hozzáadása a kontextushoz.
   * @param field Hibaüzenet hozzárendelése ehhez a mezőnévhez. Null esetén a hibaüzenet globális lesz. 
   * @param message Hibaüzenet.
   */
  public void add(String field, String message) {
    
    // Field és message paraméter ellenőrzése
    checkField(field);
    checkMessage(message);

    // Van hibaüzenet ?
    if (message == null) {
      return;
    }
    
    // Ha fieldUnique módban vagyunk, és van már hibaüzenet a mezőhöz, akkor nincs további teendő.
    if (fieldUnique) {
      if (field != null) { 
        if (bindingResult.hasFieldErrors(field)) {
          return;
        }
      }  
    } 
    
    // Hibaüzenet bejegyzése
    if (field == null) {
      bindingResult.reject(null, message);
    }
    else {
      bindingResult.rejectValue(field, null, message);
    }
  }
  
  /**
   * Mezőnév ellenőrzése: nem lehet üres string.
   * @throws RuntimeException Ha a mezőnév üres string.
   */
  private void checkField(String field) {
    if (StringTool.equals(field, "")) {
      throw new RuntimeException("A 'field' paraméter nem lehet üres string!");
    }
  }
  
  /**
   * Hibaüzenet ellenőrzése: nem lehet üres string.
   * @throws RuntimeException Ha a hibaüzenet üres string.
   */
  private void checkMessage(String message) {
    if (StringTool.equals(message, "")) {
      throw new RuntimeException("A 'message' paraméter nem lehet üres string!");
    }
  }

  // =====
}

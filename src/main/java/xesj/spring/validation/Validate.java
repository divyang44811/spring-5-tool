package xesj.spring.validation;
import lombok.NonNull;

/**
 * Validáció ősosztály. 
 * Validáció készítéskor ezt az osztályt kell leszármaztatni, és a leszármazott osztálynak csak a konstruktorát kell megírni.
 * A konstruktornak el kell végezni a validációt.
 * Adathiba miatti sikertelen validáció esetén a "message" változóba be kell állítani a hibaüzenetet.
 */
public class Validate {
  
  /** Hibaüzenet */
  protected Message message;
  
  /**
   * Hozzáadás a messages.properties fájlban lévő kódhoz, hogy a hibaüzenet szöveg meghatározásakor a Spring ezzel az új kóddal keressen.
   * @param plus A messages.properties fájlban lévő kódhoz hozzáfűzendő string. Ajánlott a stringet "+" jellel kezdeni. Null esetén nem változik a kód.
   * @return Saját maga.
   */
  public Validate messageCodePlus(String plus) {
    if (message != null) {
      message.messageCodePlus(plus);
    }  
    return this;
  }
  
  /**
   * A hibaüzenet elé írandó szöveg beállítása.
   * @param prefix A hibaüzenet elé írandó szöveg. Null esetén nem változik a hibaüzenet.
   * @return Saját maga.
   */
  public Validate messagePrefix(String prefix) {
    if (message != null) {
      message.messagePrefix(prefix);
    }
    return this;
  }

  /**
   * A hibaüzenet után írandó szöveg beállítása.
   * @param postfix A hibaüzenet után írandó szöveg. Null esetén nem változik a hibaüzenet.
   * @return Saját maga.
   */
  public Validate messagePostfix(String postfix) {
    if (message != null) {
      message.messagePostfix(postfix);
    }  
    return this;
  }
  
  /**
   * A validáció során keletkezett hibaüzenet lekérdezése. Ha nem keletkezett hibaüzenet akkor null-t ad vissza.
   * @param msl Hibaüzeneteket visszafejtő objektum.
   * @return A hibaüzenet. Ha nem keletkezett hibaüzenet akkor null.
   */
  public String getMessage(@NonNull MessageSourceLocale msl) {
    return (message == null ? null : message.getMessageText(msl));
  }

  /**
   * Validáció sikerességének ellenőrzése.
   * @return True: sikeres validáció, nem keletkezett hibaüzenet.<br/>
   *         False: sikertelen validáció, keletkezett hibaüzenet.
   */
  public boolean isValid() {
    return (message == null);
  }
  
  // =====
}

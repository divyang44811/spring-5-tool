package xesj.spring.validation;
import xesj.tool.StringTool;

/**
 * Konverzió/validáció során keletkező hibaüzenet kezelése.
 */
public class Message {
  
  /** A messages.properties fájlban kód hiányára utaló üzenet eleje */
  public static final String NOT_EXIST = "A messages.properties fájlban hiányzik a kód:";
  
  /** A messages.properties fájlban lévő hibaüzenet kulcsa */
  private String messageCode;

  /** A messages.properties fájlban lévő hibaüzenet argumentumok: {0}, {1}, ... értékei */
  private Object[] messageArgs;
  
  /** A hibaüzenet szövege */
  private String messageText;
  
  /** A hibaüzenet előtt megjelenő szöveg. Csak akkor jelenik meg, ha van hibaüzenet. */
  private String messagePrefix;

  /** A hibaüzenet után megjelenő szöveg. Csak akkor jelenik meg, ha van hibaüzenet. */
  private String messagePostfix;

  /** A messages.properties fájlban lévő hibaüzenet kulcsának kibővítése. */
  private String messageCodePlus;
  
  /**
   * Konstruktor.
   * A messageCode és messageText közül pontosan az egyik megadása kötelező.
   * @param messageCode A messages.properties fájlban lévő hibaüzenet kulcsa.
   * @param messageArgs A messages.properties fájlban lévő hibaüzenet argumentumok.
   * @param messageText Hibaüzenet szövege.
   */
  public Message(String messageCode, Object[] messageArgs, String messageText) {
    // Ellenőrzés
    if (messageCode == null && messageText == null) {
      throw new RuntimeException("A messageCode és messageText paraméter közül az egyik megadása kötelező!");
    }
    if (messageCode != null && messageText != null) {
      throw new RuntimeException("A messageCode és messageText paraméter közül mindkettő megadása tilos!");
    }
    
    // Beállítás
    this.messageCode = messageCode;
    this.messageArgs = messageArgs;
    this.messageText = messageText;
  }
  
  /**
   * A hibaüzenet kulcsának kibővítése.
   * @param plus A messages.properties fájlban lévő kódhoz hozzáfűzendő string. Ajánlott a stringet "+" jellel kezdeni. Null esetén nem változik a kód.
   * @return Saját maga.
   */
  public Message messageCodePlus(String plus) {
    // Ellenőrzés
    if (messageCode == null) {
      throw new RuntimeException("A messageCode értéke null, ezért nem bővíthető!");
    }

    // Beállítás 
    messageCodePlus = plus;
    return this;
  }
  
  /**
   * A hibaüzenet előtt megjelenő szöveg beállítása.
   * @param prefix A hibaüzenet elé írandó szöveg. Null esetén nem változik a hibaüzenet.
   * @return Saját maga.
   */
  public Message messagePrefix(String prefix) {
    this.messagePrefix = prefix;
    return this;
  }

  /**
   * A hibaüzenet után megjelenő szöveg beállítása.
   * @param postfix A hibaüzenet után írandó szöveg. Null esetén nem változik a hibaüzenet.
   * @return Saját maga.
   */
  public Message messagePostfix(String postfix) {
    this.messagePostfix = postfix;
    return this;
  }
  
  /**
   * A végleges hibaüzenet szöveg lekérdezése.
   * @param msl Hibaüzeneteket visszafejtő objektum.
   */
  public String getMessageText(MessageSourceLocale msl) {
    String text;
    if (messageCode != null) {
      String ec = messageCode + StringTool.str(messageCodePlus, "");
      String dm = NOT_EXIST + " \"" + ec + "\"";  
      text = msl.getMessageSource().getMessage(ec, messageArgs, dm, msl.getLocale());
    }
    else {
      text = messageText;
    }
    return StringTool.str(messagePrefix, "") + text + StringTool.str(messagePostfix, "");
  }
  
  // =====
}

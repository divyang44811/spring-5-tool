package xesj.spring.html;
import java.sql.ResultSet;
import java.sql.SQLException;
import xesj.shell.ResultSetConverter;

/**
 * A html &lt;select&gt; elemhez tartozó &lt;option&gt; gyerek elemeket reprezentálja.
 * Radio gombhoz is használható.
 */
public class Option {
 
  private String value;
  private String display;

  /**
   * Konstruktor
   * A value, és display értéke is "" lesz.
   */
  public Option() {
    this.value = "";
    this.display = "";
  }

  /**
   * Konstruktor
   * @param value Érték. Null esetén az értéket üres stringre konvertálja, mert csak ez jelenhet meg a webes felületen.
   * @param display Megjelenés. Null esetén a megjelenést üres stringre konvertálja, mert csak ez jelenhet meg a webes felületen.
   */
  public Option(String value, String display) {
    this.value = (value == null ? "" : value);
    this.display = (display == null ? "" : display);
  }
  
  /**
   * Shell ResultSetConverter, mely a ResultSet-et Option-re alakítja.<br/> 
   * Használat:
   *   <pre>
   *   // Az SQL-select első oszlopának a value-t, a másodiknak a display-t kell tartalmaznia.
   *   List&lt;Option&gt; optionList = shell.sql("SELECT...").getList(Option.resultSetConverter);
   *   </pre>
   */
  public static final ResultSetConverter<Option> resultSetConverter = 
    new ResultSetConverter<Option>() { 
      @Override
      public Option convert(ResultSet resultSet, int rowNumber) {
        try {
          return new Option(resultSet.getString(1), resultSet.getString(2));
        }
        catch (SQLException e) {
          throw new RuntimeException(e);
        }
      }
    };

  /**
   * Getter, setter
   */
  public String getValue() {
    return value;
  }
  public void setValue(String value) {
    this.value = (value == null ? "" : value);
  }
  public String getDisplay() {
    return display;
  }
  public void setDisplay(String display) {
    this.display = (display == null ? "" : display);
  }

  // =====
}

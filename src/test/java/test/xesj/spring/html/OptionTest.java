package test.xesj.spring.html;
import test.xesj.spring.TestConfiguration;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import xesj.spring.html.Option;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class OptionTest {

  /**
   * Teszt 
   */
  @Test
  public void test() {
    Option option;
    
    // Null érték 1. eset
    option = new Option();
    assertEquals("", option.getDisplay());
    assertEquals("", option.getValue());

    // Null érték 2. eset
    option = new Option();
    option.setValue(null);
    option.setDisplay(null);
    assertEquals("", option.getDisplay());
    assertEquals("", option.getValue());

    // Null érték 3. eset
    option = new Option(null, null);
    assertEquals("", option.getDisplay());
    assertEquals("", option.getValue());
    
    // Egyéb érték
    option = new Option("v", "D");
    assertEquals("D", option.getDisplay());
    assertEquals("v", option.getValue());
  }  
  
  // ===== 
}

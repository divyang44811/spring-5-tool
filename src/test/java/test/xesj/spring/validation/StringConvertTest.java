package test.xesj.spring.validation;
import test.xesj.spring.TestConfiguration;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.StringConvert;
import xesj.tool.LocaleTool;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class StringConvertTest {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt 
   */
  @Test
  public void messageTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);

    // Nincs adat
    assertNull(new StringConvert(null).getValue());
    assertNull(new StringConvert(null).getMessage(msl));
    
    assertNull(new StringConvert("").getValue());
    assertNull(new StringConvert("").getMessage(msl));
    
    // Van adat
    String[] texts = {" ", "éáű", "  \n \r \t ÉŐÚ   "};
    for (String text: texts) {
      assertEquals(text, new StringConvert(text).getValue());
      assertNull(new StringConvert(text).getMessage(msl));
    }
  }  
  
  // ===== 
}

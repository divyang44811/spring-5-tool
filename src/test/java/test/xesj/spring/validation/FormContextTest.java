package test.xesj.spring.validation;
import test.xesj.spring.TestConfiguration;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import xesj.spring.validation.FormContext;
import xesj.spring.validation.LongConvert;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class FormContextTest {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt 
   * BaseContext beállítása: FieldUnique = true
   */
  @Test
  public void fieldUniqueTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    FormContextObject object = new FormContextObject();
    BeanPropertyBindingResult result = new BeanPropertyBindingResult(object, "objectname");
    FormContext context = new FormContext(result, msl, true);

    // A context még üres
    context.add("kor", new LongConvert("102"));
    assertFalse(result.hasErrors());
    assertFalse(result.hasFieldErrors());
    assertFalse(result.hasFieldErrors("kor"));
    assertFalse(result.hasGlobalErrors());
    
    // A context egyetlen hibaüzenetet tartalmaz a "nev" mezőhöz
    context.add("nev", "első hibaüzenet");
    context.add("nev", "második hibaüzenet");
    assertTrue(result.hasErrors());
    assertTrue(result.hasFieldErrors());
    assertFalse(result.hasFieldErrors("kor"));
    assertTrue(result.hasFieldErrors("nev"));
    assertFalse(result.hasGlobalErrors());
    assertEquals(1, result.getFieldErrors("nev").size());
    assertEquals(0, result.getFieldErrors("kor").size());
    
    // A context a "nev" és a "kor" mezőhöz is egyetlen hibaüzenetet tartalmaz, és 3 darab globális hibaüzenetet is tárol
    context.add("kor", new LongConvert("xx"));
    context.add("kor", new LongConvert("yy"));
    context.add(null, "global-1");
    context.add(null, "global-2");
    context.add(null, "global-3");
    assertTrue(result.hasErrors());
    assertTrue(result.hasFieldErrors());
    assertTrue(result.hasFieldErrors("kor"));
    assertTrue(result.hasFieldErrors("nev"));
    assertTrue(result.hasGlobalErrors());
    assertEquals(1, result.getFieldErrors("nev").size());
    assertEquals(1, result.getFieldErrors("kor").size());
    assertEquals(3, result.getGlobalErrorCount());
  }

  /**
   * Hibaüzenet teszt 
   * BaseContext beállítása: FieldUnique = false
   */
  @Test
  public void fieldNotUniqueTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    FormContextObject object = new FormContextObject();
    BeanPropertyBindingResult result = new BeanPropertyBindingResult(object, "objectname");
    FormContext context = new FormContext(result, msl, false);

    // A context még üres
    context.add("kor", new LongConvert("102"));
    assertFalse(result.hasErrors());
    assertFalse(result.hasFieldErrors());
    assertFalse(result.hasFieldErrors("kor"));
    assertFalse(result.hasGlobalErrors());
    
    // A context két hibaüzenetet tartalmaz a "nev" mezőhöz
    context.add("nev", "első hibaüzenet");
    context.add("nev", "második hibaüzenet");
    assertTrue(result.hasErrors());
    assertTrue(result.hasFieldErrors());
    assertFalse(result.hasFieldErrors("kor"));
    assertTrue(result.hasFieldErrors("nev"));
    assertFalse(result.hasGlobalErrors());
    assertEquals(2, result.getFieldErrors("nev").size());
    assertEquals(0, result.getFieldErrors("kor").size());
    
    // A context a "nev" és a "kor" mezőhöz is kettő hibaüzenetet tartalmaz, és 3 darab globális hibaüzenetet is tárol
    context.add("kor", new LongConvert("xx"));
    context.add("kor", new LongConvert("yy"));
    context.add(null, "global-1");
    context.add(null, new Message("nemlétező kód", null, null));
    context.add(null, new Message("test.error", new String[]{"ABC"}, null).messagePrefix(">").messagePostfix("<").messageCodePlus("+pl"));
    assertTrue(result.hasErrors());
    assertTrue(result.hasFieldErrors());
    assertTrue(result.hasFieldErrors("kor"));
    assertTrue(result.hasFieldErrors("nev"));
    assertTrue(result.hasGlobalErrors());
    assertEquals(2, result.getFieldErrors("nev").size());
    assertEquals(2, result.getFieldErrors("kor").size());
    assertEquals(3, result.getGlobalErrorCount());
  }
  
  // ===== 
}

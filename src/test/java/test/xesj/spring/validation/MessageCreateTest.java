package test.xesj.spring.validation;
import test.xesj.spring.TestConfiguration;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

/**
 * Az előállított hibaüzenet tesztelése attól függően hogy módosítjuk a hibaüzenetet prefix-szel, postfix-szel, és code plus-szal.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class MessageCreateTest {
  
  @Autowired MessageSource messageSource;
  
  private static final String 
    MESSAGE_CODE = "test.error",
    DATA = "ÁRVÍZTŰRŐ";

  /**
   * Alap hibaüzenet teszt 
   */
  @Test
  public void baseTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    String message1, message2, message3, message4;

    // Alap hibaüzenet 
    message1 = messageSource.getMessage(MESSAGE_CODE, new String[]{DATA}, LocaleTool.LOCALE_HU);
    message2 = new TestConvert(DATA).getMessage(msl);
    message3 = new TestValidate(DATA).getMessage(msl);
    message4 = new TestValidateConcrete(DATA).getMessage(msl);
    assertEquals(message1, message2);
    assertEquals(message1, message3);
    assertEquals(DATA, message4);
  }  
  
  /**
   * Prefix-szel ellátott hibaüzenet teszt 
   */
  @Test
  public void prefixTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    String message1, message2, message3, message4, prefix;
    
    // Message prefix = null
    prefix = null;
    message1 = messageSource.getMessage(MESSAGE_CODE, new String[]{DATA}, LocaleTool.LOCALE_HU);
    message2 = new TestConvert(DATA).messagePrefix(prefix).getMessage(msl);
    message3 = new TestValidate(DATA).messagePrefix(prefix).getMessage(msl);
    message4 = new TestValidateConcrete(DATA).messagePrefix(prefix).getMessage(msl);
    assertEquals(message1, message2);
    assertEquals(message1, message3);
    assertEquals(DATA, message4);

    // Message prefix = ""
    prefix = "";
    message1 = messageSource.getMessage(MESSAGE_CODE, new String[]{DATA}, LocaleTool.LOCALE_HU);
    message2 = new TestConvert(DATA).messagePrefix(prefix).getMessage(msl);
    message3 = new TestValidate(DATA).messagePrefix(prefix).getMessage(msl);
    message4 = new TestValidateConcrete(DATA).messagePrefix(prefix).getMessage(msl);
    assertEquals(message1, message2);
    assertEquals(message1, message3);
    assertEquals(DATA, message4);
    
    // Message prefix = "..."
    prefix = "4. sor, 5. elem: ";
    message1 = prefix + messageSource.getMessage(MESSAGE_CODE, new String[]{DATA}, LocaleTool.LOCALE_HU);
    message2 = new TestConvert(DATA).messagePrefix(prefix).getMessage(msl);
    message3 = new TestValidate(DATA).messagePrefix(prefix).getMessage(msl);
    message4 = new TestValidateConcrete(DATA).messagePrefix(prefix).getMessage(msl);
    assertEquals(message1, message2);
    assertEquals(message1, message3);
    assertEquals(prefix + DATA, message4);
  }

  /**
   * Postfix-szel ellátott hibaüzenet teszt 
   */
  @Test
  public void postfixTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    String message1, message2, message3, message4, postfix;
  
    // Message postfix = null
    postfix = null;
    message1 = messageSource.getMessage(MESSAGE_CODE, new String[]{DATA}, LocaleTool.LOCALE_HU);
    message2 = new TestConvert(DATA).messagePostfix(postfix).getMessage(msl);
    message3 = new TestValidate(DATA).messagePostfix(postfix).getMessage(msl);
    message4 = new TestValidateConcrete(DATA).messagePostfix(postfix).getMessage(msl);
    assertEquals(message1, message2);
    assertEquals(message1, message3);
    assertEquals(DATA, message4);

    // Message postfix = ""
    postfix = "";
    message1 = messageSource.getMessage(MESSAGE_CODE, new String[]{DATA}, LocaleTool.LOCALE_HU);
    message2 = new TestConvert(DATA).messagePostfix(postfix).getMessage(msl);
    message3 = new TestValidate(DATA).messagePostfix(postfix).getMessage(msl);
    message4 = new TestValidateConcrete(DATA).messagePostfix(postfix).getMessage(msl);
    assertEquals(message1, message2);
    assertEquals(message1, message3);
    assertEquals(DATA, message4);
    
    // Message postfix = "..."
    postfix = " (4. sor, 5. elem)";
    message1 = messageSource.getMessage(MESSAGE_CODE, new String[]{DATA}, LocaleTool.LOCALE_HU) + postfix;
    message2 = new TestConvert(DATA).messagePostfix(postfix).getMessage(msl);
    message3 = new TestValidate(DATA).messagePostfix(postfix).getMessage(msl);
    message4 = new TestValidateConcrete(DATA).messagePostfix(postfix).getMessage(msl);
    assertEquals(message1, message2);
    assertEquals(message1, message3);
    assertEquals(DATA + postfix, message4);
  }
  
  /**
   * Code plus-szal ellátott hibaüzenet teszt 
   */
  @Test
  public void codePlusTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    String message1, message2, message3, message4, codePlus;
    
    // Message code plus = null
    codePlus = null;
    message1 = messageSource.getMessage(MESSAGE_CODE, new String[]{DATA}, LocaleTool.LOCALE_HU);
    message2 = new TestConvert(DATA).messageCodePlus(codePlus).getMessage(msl);
    message3 = new TestValidate(DATA).messageCodePlus(codePlus).getMessage(msl);
    assertEquals(message1, message2);
    assertEquals(message1, message3);

    // Message code plus = ""
    codePlus = "";
    message1 = messageSource.getMessage(MESSAGE_CODE, new String[]{DATA}, LocaleTool.LOCALE_HU);
    message2 = new TestConvert(DATA).messageCodePlus(codePlus).getMessage(msl);
    message3 = new TestValidate(DATA).messageCodePlus(codePlus).getMessage(msl);
    assertEquals(message1, message2);
    assertEquals(message1, message3);
    
    // Message code plus = "+pl"
    codePlus = "+pl";
    message1 = messageSource.getMessage(MESSAGE_CODE + codePlus, new String[]{DATA}, LocaleTool.LOCALE_HU);
    message2 = new TestConvert(DATA).messageCodePlus(codePlus).getMessage(msl);
    message3 = new TestValidate(DATA).messageCodePlus(codePlus).getMessage(msl);
    assertEquals(message1, message2);
    assertEquals(message1, message3);

    // Message code plus = "+ROSSZ"
    codePlus = "+ROSSZ";
    message2 = new TestConvert(DATA).messageCodePlus(codePlus).getMessage(msl);
    message3 = new TestValidate(DATA).messageCodePlus(codePlus).getMessage(msl);
    assertEquals(message2, message3);
    System.out.println("Hibaüzenet rossze kulccsal --> " + message2);
  }  
  
  /**
   * Exception teszt
   */
  @Test
  public void exceptionTest() {
    // A messageCode, messageText közül egyik megadása kötelező 
    try {
      new Message(null, null, null);
      fail();
    }  
    catch (RuntimeException re) {}

    // Nem lehet egyszerre megadni a messageCode, messageText-et 
    try {
      new Message("kód", null, "szöveg");
      fail();
    }  
    catch (RuntimeException re) {}

    // Nincs messageCode, ezért nem bővíthető 
    try {
      new Message(null, null, "szöveg").messageCodePlus("+plus");
      fail();
    }  
    catch (RuntimeException re) {}
  }
  
  // ===== 
}

package test.xesj.spring.validation;
import test.xesj.spring.TestConfiguration;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import xesj.spring.validation.IntegerRangeValidate;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class IntegerRangeValidateTest {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt. 
   */
  @Test
  public void messageTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    Integer middle = 123, less = 75, more = 477;

    // Nincs adat    
    assertNull(new IntegerRangeValidate(null, less, less).getMessage(msl));
    assertNull(new IntegerRangeValidate(null, null, less).getMessage(msl));
    assertNull(new IntegerRangeValidate(null, less, null).getMessage(msl));
    assertNull(new IntegerRangeValidate(null, null, null).getMessage(msl));
    assertTrue(new IntegerRangeValidate(null, null, null).isValid());

    // Helyes az adat
    assertNull(new IntegerRangeValidate(middle, middle, middle).getMessage(msl));
    assertNull(new IntegerRangeValidate(middle, less, more).getMessage(msl));
    assertNull(new IntegerRangeValidate(middle, null, more).getMessage(msl));
    assertNull(new IntegerRangeValidate(middle, null, middle).getMessage(msl));
    assertNull(new IntegerRangeValidate(middle, middle, null).getMessage(msl));
    assertNull(new IntegerRangeValidate(middle, middle, more).getMessage(msl));
    assertNull(new IntegerRangeValidate(more, more, more).getMessage(msl));
    assertNull(new IntegerRangeValidate(less, less, less).getMessage(msl));
    assertTrue(new IntegerRangeValidate(middle, middle, middle).isValid());

    // Hibás az adat
    assertNotNull(new IntegerRangeValidate(middle, more, less).getMessage(msl));
    assertNotNull(new IntegerRangeValidate(middle, more, more).getMessage(msl));
    assertNotNull(new IntegerRangeValidate(middle, less, less).getMessage(msl));
    assertNotNull(new IntegerRangeValidate(middle, more, null).getMessage(msl));
    assertNotNull(new IntegerRangeValidate(middle, null, less).getMessage(msl));
    assertNotNull(new IntegerRangeValidate(less, middle, middle).getMessage(msl));
    assertNotNull(new IntegerRangeValidate(less, middle, null).getMessage(msl));
    assertNotNull(new IntegerRangeValidate(more, null, middle).getMessage(msl));
    assertNotNull(new IntegerRangeValidate(more, less, middle).getMessage(msl));
    assertNotNull(new IntegerRangeValidate(more, middle, less).getMessage(msl));
    assertFalse(new IntegerRangeValidate(more, middle, less).isValid());
  }

  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new IntegerRangeValidate(5, 7, null).getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new IntegerRangeValidate(5, null, 3).getMessage(msl).startsWith(Message.NOT_EXIST));
  }
  
  // ===== 
}

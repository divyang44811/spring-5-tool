package test.xesj.spring.validation;
import test.xesj.spring.TestConfiguration;
import java.util.Date;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import xesj.spring.validation.DateRangeValidate;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class DateRangeValidateTest {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt. 
   */
  @Test
  public void messageTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    Date now = new Date(); // most
    Date past = new Date(System.currentTimeMillis() - 1800 * 1000); // most - fél óra
    Date future = new Date(System.currentTimeMillis() + 70 * 1000); // most + 70 másodperc

    // Nincs adat    
    assertNull(new DateRangeValidate(null, now, now, "yyyy.MM.dd").getMessage(msl));
    assertNull(new DateRangeValidate(null, null, now, "yyyy.MM.dd").getMessage(msl));
    assertNull(new DateRangeValidate(null, now, null, "yyyy.MM.dd").getMessage(msl));
    assertNull(new DateRangeValidate(null, null, null, "yyyy.MM.dd").getMessage(msl));

    // Helyes az adat
    assertNull(new DateRangeValidate(now, now, now, "yyyy.MM.dd").getMessage(msl));
    assertNull(new DateRangeValidate(now, past, future, "yyyy").getMessage(msl));
    assertNull(new DateRangeValidate(now, null, future, "yyyy").getMessage(msl));
    assertNull(new DateRangeValidate(now, null, now, "yyyy").getMessage(msl));
    assertNull(new DateRangeValidate(now, now, null, "yyyy").getMessage(msl));
    assertNull(new DateRangeValidate(now, now, future, "yyyy").getMessage(msl));
    assertNull(new DateRangeValidate(future, future, future, "yyyy").getMessage(msl));
    assertNull(new DateRangeValidate(past, past, past, "yyyy").getMessage(msl));

    // Hibás az adat
    assertNotNull(new DateRangeValidate(now, future, past, "yyyy").getMessage(msl));
    assertNotNull(new DateRangeValidate(now, future, future, "yyyy").getMessage(msl));
    assertNotNull(new DateRangeValidate(now, past, past, "yyyy").getMessage(msl));
    assertNotNull(new DateRangeValidate(now, future, null, "yyyy").getMessage(msl));
    assertNotNull(new DateRangeValidate(now, null, past, "yyyy").getMessage(msl));
    assertNotNull(new DateRangeValidate(past, now, now, "yyyy").getMessage(msl));
    assertNotNull(new DateRangeValidate(past, now, null, "yyyy").getMessage(msl));
    assertNotNull(new DateRangeValidate(future, null, now, "yyyy").getMessage(msl));
    assertNotNull(new DateRangeValidate(future, past, now, "yyyy").getMessage(msl));
    assertNotNull(new DateRangeValidate(future, now, past, "yyyy").getMessage(msl));
  }
  
  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    Date now = new Date(); // most
    Date past = new Date(System.currentTimeMillis() - 1800 * 1000); // most - fél óra
    Date future = new Date(System.currentTimeMillis() + 70 * 1000); // most + 70 másodperc

    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new DateRangeValidate(now, future, null, "yyyy.MM.dd").getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new DateRangeValidate(now, null, past, "yyyy.MM.dd").getMessage(msl).startsWith(Message.NOT_EXIST));
  }
  
  // ===== 
}

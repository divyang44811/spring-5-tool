package test.xesj.spring.validation;
import test.xesj.spring.TestConfiguration;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import xesj.spring.validation.Convert;
import xesj.spring.validation.LongConvert;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;
import xesj.tool.StringTool;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class LongConvertTest {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt 
   */
  @Test
  public void messageTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);

    // Nincs adat
    assertNull(new LongConvert(null).getValue());
    assertNull(new LongConvert(null).getMessage(msl));
    assertTrue(new LongConvert(null).isValid());
    
    assertNull(new LongConvert("").getValue());
    assertNull(new LongConvert("").getMessage(msl));
    assertTrue(new LongConvert("").isValid());
    
    // Helyes az adat
    assertEquals(0L, new LongConvert("0").getValue());
    assertNull(new LongConvert("0").getMessage(msl));
    assertTrue(new LongConvert("0").isValid());

    assertEquals(-123L, new LongConvert("-123").getValue());
    assertNull(new LongConvert("-123").getMessage(msl));
    assertTrue(new LongConvert("-123").isValid());

    assertEquals(Long.MIN_VALUE, new LongConvert(StringTool.str(Long.MIN_VALUE)).getValue());
    assertNull(new LongConvert(StringTool.str(Long.MIN_VALUE)).getMessage(msl));
    assertTrue(new LongConvert(StringTool.str(Long.MIN_VALUE)).isValid());

    assertEquals(Long.MAX_VALUE, new LongConvert(StringTool.str(Long.MAX_VALUE)).getValue());
    assertNull(new LongConvert(StringTool.str(Long.MAX_VALUE)).getMessage(msl));
    assertTrue(new LongConvert(StringTool.str(Long.MAX_VALUE)).isValid());

    // Hibás az adat (hibaüzenet kiírással)
    String[] texts = {"xyz", "  7", "3 0", "3.1", "99999999999999999999", "X999999999Z99999999Y", "9999999999.999999999"}; 
    for (String text: texts) {
      Convert convert = new LongConvert(text);
      String message = convert.getMessage(msl); 
      assertNotNull(message);
      assertFalse(convert.isValid());
      try {
        convert.getValue();
        fail();
      }
      catch (RuntimeException re) {
      }
      System.out.println(text + " --> " + message);
    }
  }

  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new LongConvert("a").getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new LongConvert("999999999999999999999999999999").getMessage(msl).startsWith(Message.NOT_EXIST));
  }
  
  // ===== 
}

package test.xesj.spring.validation;
import java.math.BigInteger;
import test.xesj.spring.TestConfiguration;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import xesj.spring.validation.BigIntegerRangeValidate;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class BigIntegerRangeValidateTest {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt. 
   */
  @Test
  public void messageTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    BigInteger middle = new BigInteger("123"), less = new BigInteger("75"), more = new BigInteger("477");

    // Nincs adat    
    assertNull(new BigIntegerRangeValidate(null, less, less).getMessage(msl));
    assertNull(new BigIntegerRangeValidate(null, null, less).getMessage(msl));
    assertNull(new BigIntegerRangeValidate(null, less, null).getMessage(msl));
    assertNull(new BigIntegerRangeValidate(null, null, null).getMessage(msl));
    assertTrue(new BigIntegerRangeValidate(null, null, null).isValid());

    // Helyes az adat
    assertNull(new BigIntegerRangeValidate(middle, middle, middle).getMessage(msl));
    assertNull(new BigIntegerRangeValidate(middle, less, more).getMessage(msl));
    assertNull(new BigIntegerRangeValidate(middle, null, more).getMessage(msl));
    assertNull(new BigIntegerRangeValidate(middle, null, middle).getMessage(msl));
    assertNull(new BigIntegerRangeValidate(middle, middle, null).getMessage(msl));
    assertNull(new BigIntegerRangeValidate(middle, middle, more).getMessage(msl));
    assertNull(new BigIntegerRangeValidate(more, more, more).getMessage(msl));
    assertNull(new BigIntegerRangeValidate(less, less, less).getMessage(msl));
    assertTrue(new BigIntegerRangeValidate(middle, middle, middle).isValid());

    // Hibás az adat
    assertNotNull(new BigIntegerRangeValidate(middle, more, less).getMessage(msl));
    assertNotNull(new BigIntegerRangeValidate(middle, more, more).getMessage(msl));
    assertNotNull(new BigIntegerRangeValidate(middle, less, less).getMessage(msl));
    assertNotNull(new BigIntegerRangeValidate(middle, more, null).getMessage(msl));
    assertNotNull(new BigIntegerRangeValidate(middle, null, less).getMessage(msl));
    assertNotNull(new BigIntegerRangeValidate(less, middle, middle).getMessage(msl));
    assertNotNull(new BigIntegerRangeValidate(less, middle, null).getMessage(msl));
    assertNotNull(new BigIntegerRangeValidate(more, null, middle).getMessage(msl));
    assertNotNull(new BigIntegerRangeValidate(more, less, middle).getMessage(msl));
    assertNotNull(new BigIntegerRangeValidate(more, middle, less).getMessage(msl));
    assertFalse(new BigIntegerRangeValidate(more, middle, less).isValid());
  }

  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new BigIntegerRangeValidate(new BigInteger("5"), new BigInteger("7"), null).getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new BigIntegerRangeValidate(new BigInteger("5"), null, new BigInteger("3")).getMessage(msl).startsWith(Message.NOT_EXIST));
    System.out.println("HIBAÜZENET: " + new BigIntegerRangeValidate(new BigInteger("5"), new BigInteger("987654321098765432109876543210"), null).getMessage(msl));
  }
  
  // ===== 
}

package test.xesj.spring.validation;
import test.xesj.spring.TestConfiguration;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import xesj.spring.validation.DoubleRangeValidate;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class DoubleRangeValidateTest {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt. 
   */
  @Test
  public void messageTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    Double middle = 123.45, less = 75.12, more = 477.98;

    // Nincs adat    
    assertNull(new DoubleRangeValidate(null, less, less).getMessage(msl));
    assertNull(new DoubleRangeValidate(null, null, less).getMessage(msl));
    assertNull(new DoubleRangeValidate(null, less, null).getMessage(msl));
    assertNull(new DoubleRangeValidate(null, null, null).getMessage(msl));
    assertTrue(new DoubleRangeValidate(null, null, null).isValid());

    // Helyes az adat
    assertNull(new DoubleRangeValidate(middle, middle, middle).getMessage(msl));
    assertNull(new DoubleRangeValidate(middle, less, more).getMessage(msl));
    assertNull(new DoubleRangeValidate(middle, null, more).getMessage(msl));
    assertNull(new DoubleRangeValidate(middle, null, middle).getMessage(msl));
    assertNull(new DoubleRangeValidate(middle, middle, null).getMessage(msl));
    assertNull(new DoubleRangeValidate(middle, middle, more).getMessage(msl));
    assertNull(new DoubleRangeValidate(more, more, more).getMessage(msl));
    assertNull(new DoubleRangeValidate(less, less, less).getMessage(msl));
    assertTrue(new DoubleRangeValidate(middle, middle, middle).isValid());

    // Hibás az adat
    assertNotNull(new DoubleRangeValidate(middle, more, less).getMessage(msl));
    assertNotNull(new DoubleRangeValidate(middle, more, more).getMessage(msl));
    assertNotNull(new DoubleRangeValidate(middle, less, less).getMessage(msl));
    assertNotNull(new DoubleRangeValidate(middle, more, null).getMessage(msl));
    assertNotNull(new DoubleRangeValidate(middle, null, less).getMessage(msl));
    assertNotNull(new DoubleRangeValidate(less, middle, middle).getMessage(msl));
    assertNotNull(new DoubleRangeValidate(less, middle, null).getMessage(msl));
    assertNotNull(new DoubleRangeValidate(more, null, middle).getMessage(msl));
    assertNotNull(new DoubleRangeValidate(more, less, middle).getMessage(msl));
    assertNotNull(new DoubleRangeValidate(more, middle, less).getMessage(msl));
    assertFalse(new DoubleRangeValidate(more, middle, less).isValid());
  }

  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new DoubleRangeValidate(5.2, 5.3, null).getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new DoubleRangeValidate(5.7, null, 5.6).getMessage(msl).startsWith(Message.NOT_EXIST));
    System.out.println("HIBAÜZENET: " + new DoubleRangeValidate(5.7, 5.89, null).getMessage(msl));
  }
  
  // ===== 
}

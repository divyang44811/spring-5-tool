package test.xesj.spring.validation;
import test.xesj.spring.TestConfiguration;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.RegexpValidate;
import xesj.spring.validation.Validate;
import xesj.tool.LocaleTool;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class RegexpValidateTest {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt 
   */
  @Test
  public void messageTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);

    // Nincs adat
    assertNull(new RegexpValidate(null, null, null).getMessage(msl));
    assertNull(new RegexpValidate(null, "abc", null).getMessage(msl));
    assertNull(new RegexpValidate("abc", null, null).getMessage(msl));

    assertNull(new RegexpValidate("", "").getMessage(msl));
    assertNull(new RegexpValidate("", "abc").getMessage(msl));
    assertNull(new RegexpValidate("abc", "").getMessage(msl));
    
    // Helyes az adat
    assertNull(new RegexpValidate("1234", "[0-4]{4}", null).getMessage(msl));
    assertNull(new RegexpValidate("AB876", "[BA]{2}876|xc").getMessage(msl));
    assertNull(new RegexpValidate("xc", "[BA]{2}876|xc").getMessage(msl));

    // Hibás az adat (hibaüzenet kiírással)
    Validate validate = new RegexpValidate("69?", "[0-9]{3}", "3 darab számjegy");
    assertNotNull(validate.getMessage(msl));
    System.out.println("Hibaüzenet alap                 --> " + validate.getMessage(msl));
    System.out.println("Hibaüzenet regexp kiírással     --> " + validate.messageCodePlus("+regexp").getMessage(msl));
    System.out.println("Hibaüzenet regexp magyarázattal --> " + validate.messageCodePlus("+regexpDesc").getMessage(msl));
  }
  
  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new RegexpValidate("a", "b").getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new RegexpValidate("a", "b").messageCodePlus("+regexp").getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new RegexpValidate("a", "b").messageCodePlus("+regexpDesc").getMessage(msl).startsWith(Message.NOT_EXIST));
  }
  
  // ===== 
}

package test.xesj.spring.validation;
import xesj.spring.validation.Message;
import xesj.spring.validation.Validate;

public class TestValidate extends Validate {
  
  /**
   * Konstruktor
   */
  public TestValidate(String data) {
    message = new Message("test.error", new String[]{data}, null);
  }
  
  // =====
}

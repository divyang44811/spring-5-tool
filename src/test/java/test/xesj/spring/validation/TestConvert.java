package test.xesj.spring.validation;
import xesj.spring.validation.Convert;
import xesj.spring.validation.Message;

public class TestConvert extends Convert {
  
  /**
   * Konstruktor
   */
  public TestConvert(String data) {
    message = new Message("test.error", new String[]{data}, null);
  }
  
  // =====
}

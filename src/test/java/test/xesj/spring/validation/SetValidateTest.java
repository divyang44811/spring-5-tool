package test.xesj.spring.validation;
import test.xesj.spring.TestConfiguration;
import java.awt.Color;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.SetValidate;
import xesj.tool.LocaleTool;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class SetValidateTest {

  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt. 
   */
  @Test
  public void messageTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);

    // Nincs adat
    assertNull(new SetValidate(null, "X", "Y").getMessage(msl));
    assertNull(new SetValidate(null, Color.RED).getMessage(msl));
    assertNull(new SetValidate("", "X", "Y").getMessage(msl));

    // Helyes az adat
    assertNull(new SetValidate("egy", "kettő", "egy", " ").getMessage(msl));
    assertNull(new SetValidate("egy", new Object[]{"kettő", "egy"}).getMessage(msl));
    assertNull(new SetValidate(Color.RED, Color.GREEN, Color.RED, Color.BLUE).getMessage(msl));
    assertNull(new SetValidate(48, 13, 22, 48).getMessage(msl));
    assertNull(new SetValidate(48, (Object[])null).getMessage(msl));

    // Hibás az adat
    assertNotNull(new SetValidate("Z", "X", "Y").getMessage(msl));
    assertNotNull(new SetValidate("Z", "ZZ", "ZZZ").getMessage(msl));
    assertNotNull(new SetValidate(Color.RED, Color.GREEN, Color.BLUE).getMessage(msl));
    assertNotNull(new SetValidate(48, 13, 22).getMessage(msl));
  }
  
  /**
   * Exception tesztek
   */
  @Test
  public void exceptionTest() {

    // Az 'enabled' paraméter elemének típusa nem egyezik a vizsgálandó adat típusával
    try {
      new SetValidate(12, 12L);
      fail();
    }
    catch (RuntimeException e) {}

    // Az 'enabled' paraméter eleme nem lehet null!
    try {
      new SetValidate("A", "A", null);
      fail();
    }
    catch (RuntimeException e) {}

    // Az 'enabled' paraméter eleme nem lehet üres string!
    try {
      new SetValidate("A", "A", "");
      fail();
    }
    catch (RuntimeException e) {}
  }
  
  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new SetValidate("a", "b").getMessage(msl).startsWith(Message.NOT_EXIST));
  }
  
  // ===== 
}

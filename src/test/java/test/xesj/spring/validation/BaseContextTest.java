package test.xesj.spring.validation;
import test.xesj.spring.TestConfiguration;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import xesj.spring.validation.BaseContext;
import xesj.spring.validation.BaseContextException;
import xesj.spring.validation.DateConvert;
import xesj.spring.validation.LongConvert;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class BaseContextTest {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt 
   * BaseContext beállítása: FieldUnique = true, ThrowException = false    
   */
  @Test
  public void fieldUniqueTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    BaseContext context = new BaseContext(msl, true, null);

    // A context még üres
    context.add("m1", new LongConvert("123"));
    assertTrue(context.getFlag());
    assertFalse(context.hasFieldMessage());
    assertFalse(context.hasFieldMessage("m1"));
    assertFalse(context.hasGlobalMessage());
    assertFalse(context.hasMessage());
    assertTrue(context.getAllMessages().isEmpty());
    assertTrue(context.getFieldMessages("m1").isEmpty());
    assertTrue(context.getGlobalMessages().isEmpty());
    assertTrue(context.getFields().isEmpty());
    assertTrue(context.getAllFields().isEmpty());

    // A context egyetlen hibaüzenetet tartalmaz az "m1" mezőhöz
    context.add("m1", new LongConvert("x"));
    context.add("m1", new LongConvert("y"));
    assertFalse(context.getFlag());
    assertTrue(context.hasFieldMessage());
    assertTrue(context.hasFieldMessage("m1"));
    assertFalse(context.hasGlobalMessage());
    assertTrue(context.hasMessage());
    assertEquals(1, context.getAllMessages().size());
    assertEquals(1, context.getFieldMessages("m1").size());
    assertEquals(0, context.getFieldMessages("m2").size());
    assertTrue(context.getGlobalMessages().isEmpty());
    assertEquals(1, context.getFields().size());
    assertEquals("m1", context.getFields().get(0));
    assertEquals(1, context.getAllFields().size());
    
    // A context az "m1" és az "m2" mezőhöz is egyetlen hibaüzenetet tartalmaz, és 3 darab globális hibaüzenetet is tárol
    context.add("m2", new DateConvert("ww", "ww"));
    context.add("m2", new LongConvert("qq"));
    context.add(null, "global-1");
    context.add(null, "global-2");
    context.add(null, new Message("test.error", new String[]{"ABC"}, null).messagePrefix(">").messagePostfix("<").messageCodePlus("+pl"));
    assertFalse(context.getFlag());
    context.setFlag(true);
    assertTrue(context.getFlag());
    assertTrue(context.hasFieldMessage());
    assertTrue(context.hasFieldMessage("m1"));
    assertTrue(context.hasFieldMessage("m2"));
    assertTrue(context.hasGlobalMessage());
    assertTrue(context.hasMessage());
    assertEquals(5, context.getAllMessages().size());
    assertEquals(1, context.getFieldMessages("m1").size());
    assertEquals(1, context.getFieldMessages("m2").size());
    assertEquals(0, context.getFieldMessages("m3").size());
    assertEquals(3, context.getGlobalMessages().size());
    assertEquals(2, context.getFields().size());
    assertEquals(5, context.getAllFields().size());
    assertEquals("m1", context.getFields().get(0));
    assertEquals("m2", context.getFields().get(1));
    assertEquals("global-1", context.getGlobalMessages().get(0));
    assertEquals("global-2", context.getGlobalMessages().get(1));
    assertEquals(">Teszt hiba PL: ABC<", context.getGlobalMessages().get(2));
  }

  /**
   * Hibaüzenet teszt 
   * BaseContext beállítása: FieldUnique = false, ThrowException = false    
   */
  @Test
  public void fieldNotUniqueTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    BaseContext context = new BaseContext(msl, false, null);

    // A context még üres
    context.add("m1", new LongConvert("123"));
    assertTrue(context.getFlag());
    assertFalse(context.hasFieldMessage());
    assertFalse(context.hasFieldMessage("m1"));
    assertFalse(context.hasGlobalMessage());
    assertFalse(context.hasMessage());
    assertTrue(context.getAllMessages().isEmpty());
    assertTrue(context.getFieldMessages("m1").isEmpty());
    assertTrue(context.getGlobalMessages().isEmpty());
    assertTrue(context.getFields().isEmpty());
    assertTrue(context.getAllFields().isEmpty());

    // A context két hibaüzenetet tartalmaz az "m1" mezőhöz
    context.add("m1", new LongConvert("x"));
    context.add("m1", new LongConvert("y"));
    assertFalse(context.getFlag());
    assertTrue(context.hasFieldMessage());
    assertTrue(context.hasFieldMessage("m1"));
    assertFalse(context.hasGlobalMessage());
    assertTrue(context.hasMessage());
    assertEquals(2, context.getAllMessages().size());
    assertEquals(2, context.getFieldMessages("m1").size());
    assertEquals(0, context.getFieldMessages("m2").size());
    assertTrue(context.getGlobalMessages().isEmpty());
    assertEquals(1, context.getFields().size());
    assertEquals("m1", context.getFields().get(0));
    assertEquals(2, context.getAllFields().size());
    
    // A context az "m1" és az "m2" mezőhöz is két hibaüzenetet tartalmaz, és 3 darab globális hibaüzenetet is tárol
    context.add("m2", new DateConvert("ww", "ww"));
    context.add("m2", new LongConvert("qq"));
    context.add("m2", new LongConvert("-10000"));
    context.add(null, "global-1");
    context.add(null, "global-2");
    context.add(null, "global-3");
    assertFalse(context.getFlag());
    context.setFlag(true);
    assertTrue(context.getFlag());
    assertTrue(context.hasFieldMessage());
    assertTrue(context.hasFieldMessage("m1"));
    assertTrue(context.hasFieldMessage("m2"));
    assertTrue(context.hasGlobalMessage());
    assertTrue(context.hasMessage());
    assertEquals(7, context.getAllMessages().size());
    assertEquals(2, context.getFieldMessages("m1").size());
    assertEquals(2, context.getFieldMessages("m2").size());
    assertEquals(0, context.getFieldMessages("m3").size());
    assertEquals(3, context.getGlobalMessages().size());
    assertEquals(2, context.getFields().size());
    assertEquals(7, context.getAllFields().size());
    assertEquals("m1", context.getFields().get(0));
    assertEquals("m2", context.getFields().get(1));
    assertEquals("global-1", context.getGlobalMessages().get(0));
    assertEquals("global-2", context.getGlobalMessages().get(1));
    assertEquals("global-3", context.getGlobalMessages().get(2));
  }
  
  /**
   * Hibaüzenet teszt 
   * BaseContext beállítása: FieldUnique = false, ThrowException = true    
   */
  @Test
  public void throwExceptionTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);

    // Exception keletkezése az első mező hibaüzenetkor
    BaseContext context = new BaseContext(msl, false, 1L);
    context.add("nem-hiba", new LongConvert("8"));
    try {
      context.add("m1", new LongConvert("xx"));
      fail();
    }
    catch (BaseContextException bce) {
      assertEquals(1, context.getAllFields().size());
      assertEquals(1, context.getAllMessages().size());
    }

    // Exception keletkezése a harmadik globális hibaüzenetkor
    context = new BaseContext(msl, false, 3L);
    context.add("nem-hiba", new LongConvert("8"));
    context.add(null, new LongConvert("első hiba"));
    context.add(null, new LongConvert("második hiba"));
    try {
      context.add(null, new LongConvert("harmadik hiba"));
      fail();
    }
    catch (BaseContextException bce) {
      assertEquals(3, context.getAllFields().size());
      assertEquals(3, context.getAllMessages().size());
    }

    // Exception keletkezése a második statikus hibaüzenetkor
    context = new BaseContext(msl, false, 2L);
    context.add("nem-hiba", new LongConvert("8"));
    context.add("m1", "m1 hiba");
    try {
      context.add("m2", "m2 hiba");
      fail();
    }
    catch (BaseContextException bce) {
      assertEquals(2, context.getAllFields().size());
      assertEquals(2, context.getAllMessages().size());
      assertEquals("m1", bce.getBaseContext().getAllFields().get(0));
      assertEquals("m1 hiba", bce.getBaseContext().getAllMessages().get(0));
      assertEquals("m2", bce.getBaseContext().getAllFields().get(1));
      assertEquals("m2 hiba", bce.getBaseContext().getAllMessages().get(1));
    }
  }  
  
  /**
   * Megjegyzés teszt 
   */
  @Test
  public void commentTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    try {
      BaseContext context = new BaseContext(msl, false, 1L);
      assertNull(context.getComment());
      context.setComment("árvíztűrő komment");
      assertEquals("árvíztűrő komment", context.getComment());
      context.add(null, "hiba");
      fail();
    }  
    catch (BaseContextException bce) {
      assertEquals("árvíztűrő komment", bce.getBaseContext().getComment());
    }
  }

  /**
   * Exception dobása teszt
   */
  @Test
  public void throwExceptionIfDirtyTest() {
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    BaseContext context = new BaseContext(msl, false, 2L);
    context.throwExceptionIfDirty();
    context.add(null, "hiba");
    try {
      context.throwExceptionIfDirty();
      fail();      
    }  
    catch (BaseContextException bce) {
      assertEquals(1, bce.getBaseContext().getAllMessages().size());
    }
  }
  
  // ===== 
}

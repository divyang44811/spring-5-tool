package test.xesj.spring.property_editor;
import java.text.ParseException;
import java.util.Date;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import test.xesj.spring.TestConfiguration;
import xesj.spring.property_editor.DatePropertyEditor;
import xesj.tool.DateTool;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class DatePropertyEditorTest {

  /**
   * setAsText() metódus teszt 
   */
  @Test
  public void setAsTextTest() throws ParseException {
    
    // Pattern = null
    try {
      new DatePropertyEditor(null);
      fail();
    }  
    catch (NullPointerException e) {
    }  
    
    // Helyes adatok
    String[] pt = {"yyyy.MM.dd", "2018.11.22", "yyyy", "1968", "dd-MM-yyyy", "31-12-2008"};
    for (int i = 0; i < pt.length; i += 2) {
      DatePropertyEditor editor = new DatePropertyEditor(pt[i]);
      editor.setAsText(pt[i + 1]);
      assertEquals(DateTool.parse(pt[i + 1], pt[i]), (Date)editor.getValue());
    }
    
    // Hibás adatok, IllegalArgumentException-t várunk
    pt = new String[]{"yyyy.MM.dd", "2018.22.11", "yyMM", "1968", "dd-MM-yyyy", "29-02-2017"};
    for (int i = 0; i < pt.length; i += 2) {
      DatePropertyEditor editor = new DatePropertyEditor(pt[i]);
      try {
        editor.setAsText(pt[i + 1]);
        fail();
      }
      catch (IllegalArgumentException e) {
      }  
    }
  }  
  
  /**
   * getAsText() metódus teszt 
   */
  @Test
  public void getAsTextTest() throws ParseException {
    String[] patterns = {"yyyy.MM.dd", "yyyy", "dd-MM-yyyy"};
    Date date = new Date();
    for (String pattern: patterns) {
      DatePropertyEditor editor = new DatePropertyEditor(pattern);
      editor.setValue(date);
      assertEquals(DateTool.format(date, pattern), editor.getAsText());
    }
  }
  
  // ===== 
}
